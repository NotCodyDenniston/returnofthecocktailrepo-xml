package com.example.mealdbapp.model.local.meal

data class MealResponse(
    val meals: List<Meal>
)