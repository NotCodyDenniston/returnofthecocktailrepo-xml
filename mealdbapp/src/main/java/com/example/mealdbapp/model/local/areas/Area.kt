package com.example.mealdbapp.model.local.areas

data class Area(
    val strArea: String
)