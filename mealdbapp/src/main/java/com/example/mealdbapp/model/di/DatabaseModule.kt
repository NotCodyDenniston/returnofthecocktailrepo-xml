package com.example.mealdbapp.model.di

import com.example.mealdbapp.model.remote.MealService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    fun ProvidesRetrofit(): MealService {
        return MealService()
    }
}