package com.example.mealdbapp.model.local.categorymeals

data class CategoryMealsResponse(
    val meals: List<CategoryMeal>
)