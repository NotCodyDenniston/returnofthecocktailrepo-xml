package com.example.mealdbapp.model.local.categorymeals

data class CategoryMeal(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)