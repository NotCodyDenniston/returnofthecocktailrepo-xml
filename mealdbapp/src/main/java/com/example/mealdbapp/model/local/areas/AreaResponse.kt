package com.example.mealdbapp.model.local.areas

data class AreaResponse(
    val meals: List<Area>
)