package com.example.mealdbapp.model.local.ingredients

data class Ingredient(
    val idIngredient: String,
    val strDescription: String = "",
    val strIngredient: String = "",
    val strType: String
)