package com.example.mealdbapp.model.local.ingredients

data class IngredientsResponse(
    val meals: List<Ingredient>
)