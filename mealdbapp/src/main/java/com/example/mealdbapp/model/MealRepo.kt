package com.example.mealdbapp.model

import com.example.mealdbapp.model.local.areas.AreaResponse
import com.example.mealdbapp.model.local.category.CategoryResponse
import com.example.mealdbapp.model.local.categorymeals.CategoryMealsResponse
import com.example.mealdbapp.model.local.ingredients.IngredientsResponse
import com.example.mealdbapp.model.local.meal.MealResponse
import com.example.mealdbapp.model.remote.MealService
import javax.inject.Inject

class MealRepo @Inject constructor(
    private val service: MealService
) {

    suspend fun getCategories(): CategoryResponse{
        return service.getMealCategories()
    }

    suspend fun getMealsInCategory(category:String): CategoryMealsResponse{
        return service.getMealsInCategory(category)
    }

    suspend fun getMeal(Meal:String): MealResponse{
        return service.getMeal(Meal)
    }

    suspend fun getMealByArea(area:String): CategoryMealsResponse{
        return service.getMealsByArea(area)
    }

    suspend fun getAreas(): AreaResponse{
        return service.getAreas()
    }

    suspend fun getIngredients(): IngredientsResponse{
        return service.getIngredients()
    }
}