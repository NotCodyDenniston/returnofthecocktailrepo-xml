package com.example.mealdbapp.model.remote

import com.example.mealdbapp.model.local.areas.AreaResponse
import com.example.mealdbapp.model.local.category.CategoryResponse
import com.example.mealdbapp.model.local.categorymeals.CategoryMealsResponse
import com.example.mealdbapp.model.local.ingredients.IngredientsResponse
import com.example.mealdbapp.model.local.meal.MealResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface MealService {

    @GET(MEALCATEGORIES_ENDPOINT)
    suspend fun getMealCategories(): CategoryResponse

    @GET(MEALS_IN_CATEGORY_ENDPOINT)
    suspend fun getMealsInCategory(@Query("c")c:String = "Seafood"): CategoryMealsResponse

    @GET(MEALS_ENDPOINT)
    suspend fun getMeal(@Query("s")s:String = "Arrabiata"): MealResponse

    @GET(FILTER_BY_AREA_ENDPOINT)
    suspend fun getMealsByArea(@Query("a")a:String = "Canadian"): CategoryMealsResponse

    @GET(GET_ALL_AREAS_ENDPOINT)
    suspend fun getAreas(@Query("a")a:String = "list"): AreaResponse

    @GET(GET_ALL_INGREDIENTS_ENDPOINT)
    suspend fun getIngredients(@Query("i")i:String = "list"): IngredientsResponse

    companion object {
        const val MEALCATEGORIES_ENDPOINT = "categories.php"
        const val MEALS_IN_CATEGORY_ENDPOINT = "filter.php"
        const val MEALS_ENDPOINT = "search.php"
        const val FILTER_BY_AREA_ENDPOINT = "filter.php"
        const val GET_ALL_AREAS_ENDPOINT = "list.php"
        const val GET_ALL_INGREDIENTS_ENDPOINT = "list.php"

        const val baseurl = "https://www.themealdb.com/api/"
        const val version = "json/v1/1/"


        operator fun invoke(): MealService {

            val retrofit: Retrofit = Retrofit
                .Builder()
                .baseUrl(baseurl + version)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }
}