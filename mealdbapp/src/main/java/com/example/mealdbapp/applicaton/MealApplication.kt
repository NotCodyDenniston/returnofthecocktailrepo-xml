package com.example.mealdbapp.applicaton

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MealApplication : Application()