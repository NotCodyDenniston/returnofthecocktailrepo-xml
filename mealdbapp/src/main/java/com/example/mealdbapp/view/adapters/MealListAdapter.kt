package com.example.mealdbapp.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.categorymeals.CategoryMeal
import com.example.mealdbapp.viewmodel.CategoryState
import kotlinx.android.synthetic.main.content_main.view.category_main_description
import kotlinx.android.synthetic.main.content_main.view.category_main_title
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_image
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_title

class MealListAdapter(
    private val onMealClicked: (String) -> Unit,
) : RecyclerView.Adapter<MealListAdapter.MealListViewHolder>() {

    private val currentList: MutableList<CategoryMeal> = mutableListOf()

    fun newList(newList: List<CategoryMeal>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class MealListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.meal_list_fragment, parent, false)
        return MealListViewHolder(view)
    }

    override fun onBindViewHolder(holder: MealListViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val selectedMeal = currentList[holder.adapterPosition]
            onMealClicked(selectedMeal.strMeal)
        }
        holder.itemView.apply {
            meal_card_title.text = currentList[position].strMeal
            Glide.with(holder.itemView.rootView.context)
                .load(currentList[position].strMealThumb)
                .centerCrop()
                .into(holder.itemView.meal_card_image)
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}