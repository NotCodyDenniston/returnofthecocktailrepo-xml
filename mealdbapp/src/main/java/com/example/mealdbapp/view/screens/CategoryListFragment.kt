package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.model.local.category.Category
import com.example.mealdbapp.view.adapters.CategoryListAdapter
import com.example.mealdbapp.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class CategoryListFragment : Fragment() {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val adapter by lazy {
        CategoryListAdapter({ category ->
            categoryClicked(category)
        })
    }
    private lateinit var binding: ContentMainBinding

    // This property is only valid between onCreateView and
    // onDestroyView.

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    fun initObservers() {
        mainViewModel.getCategories()
        mainViewModel.categoryState.observe(viewLifecycleOwner) { newlist ->
            adapter.newList(newList = newlist.categories)
        }
    }

    fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@CategoryListFragment.requireContext())

    }

    fun categoryClicked(category: Category) {
        mainViewModel.getCategoryMeals(category)
        findNavController().navigate(R.id.MealListFragment)
    }
}

