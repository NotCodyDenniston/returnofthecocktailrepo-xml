package com.example.mealdbapp.view

import androidx.appcompat.app.AppCompatActivity
import com.example.mealdbapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)

// TODO LATER:
// Try to switch from activityViewModel to viewModel
// refactor names to fit better
// try to reduce unnecessary code