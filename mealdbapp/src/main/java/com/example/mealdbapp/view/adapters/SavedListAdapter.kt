package com.example.mealdbapp.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.meal.Meal
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_image
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_title

class SavedListAdapter(
    private val onMealClicked: (String) -> Unit,
) : RecyclerView.Adapter<SavedListAdapter.SavedListViewHolder>() {

    private val currentList: MutableList<Meal> = mutableListOf()

    fun newList(newList: List<Meal>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class SavedListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.meal_list_fragment, parent, false)
        return SavedListViewHolder(view)
    }

    override fun onBindViewHolder(holder: SavedListViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val selectedMeal = currentList[holder.adapterPosition]
            onMealClicked(selectedMeal.strMeal)
        }
        holder.itemView.apply {
            meal_card_title.text = currentList[position].strMeal
            Glide.with(holder.itemView.rootView.context)
                .load(currentList[position].strMealThumb)
                .centerCrop()
                .into(holder.itemView.meal_card_image)
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}