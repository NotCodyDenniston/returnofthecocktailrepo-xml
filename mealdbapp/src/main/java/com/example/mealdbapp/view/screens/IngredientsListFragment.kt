package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.view.adapters.IngredientsListAdapter
import com.example.mealdbapp.view.adapters.SavedListAdapter
import com.example.mealdbapp.viewmodel.MainViewModel

class IngredientsListFragment : Fragment() {

    private val adapter = IngredientsListAdapter()
    private val mainViewModel: MainViewModel by activityViewModels()
    private lateinit var binding: ContentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            binding.categoryMainTitle.text = "Ingredients List"
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    fun initObservers() {
        mainViewModel.getIngredients()
        mainViewModel.categoryState.observe(viewLifecycleOwner) { newState ->
            adapter.newList(newList = newState.ingredients)
        }
    }

    fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@IngredientsListFragment.requireContext())
    }

}