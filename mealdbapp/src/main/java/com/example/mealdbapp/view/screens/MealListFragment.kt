package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.view.adapters.MealListAdapter
import com.example.mealdbapp.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class MealListFragment : Fragment() {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val adapter by lazy {
        MealListAdapter({ meal ->
            mealClicked(meal)
        })
    }
    private lateinit var binding: ContentMainBinding

    // This property is only valid between onCreateView and
    // onDestroyView.

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            if(mainViewModel.categoryState.value?.areasMealListBeingObserved == false){
            binding.categoryMainTitle.text =
                mainViewModel.categoryState.value?.selectedCategory?.strCategory
            binding.categoryMainDescription.text =
                mainViewModel.categoryState.value?.selectedCategory?.strCategoryDescription
            } else {
                binding.categoryMainTitle.text =
                    mainViewModel.categoryState.value?.selectedArea
                binding.categoryMainDescription.text = "${mainViewModel.categoryState.value?.selectedArea} recipies"
            }
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    fun initObservers() {
        mainViewModel.categoryState.observe(viewLifecycleOwner) { newState ->
            adapter.newList(newList = newState.meals)
        }
    }

    fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@MealListFragment.requireContext())

    }

    fun mealClicked(meal: String) {
        mainViewModel.getMeal(meal)
        findNavController().navigate(R.id.MealFragment)
    }
}

