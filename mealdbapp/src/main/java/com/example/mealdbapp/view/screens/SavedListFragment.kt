package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.view.adapters.SavedListAdapter
import com.example.mealdbapp.viewmodel.MainViewModel

class SavedListFragment : Fragment() {

    private val adapter = SavedListAdapter({
        mealClicked(it)
    })
    private val mainViewModel: MainViewModel by activityViewModels()
    private lateinit var binding: ContentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            binding.categoryMainTitle.text = "Saved Meals"
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    fun initObservers() {
        mainViewModel.categoryState.observe(viewLifecycleOwner) { newState ->
            adapter.newList(newList = newState.savedMeals)
        }
    }

    fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@SavedListFragment.requireContext())
    }

    fun mealClicked(meal: String) {
        mainViewModel.getMeal(meal)
        findNavController().navigate(R.id.MealFragment)
    }
}