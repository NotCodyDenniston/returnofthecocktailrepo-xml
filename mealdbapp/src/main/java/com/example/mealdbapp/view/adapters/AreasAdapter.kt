package com.example.mealdbapp.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.areas.Area
import kotlinx.android.synthetic.main.area_fragment.view.area_text_box

class AreasAdapter(
    private val onAreaClicked: (String) -> Unit
): RecyclerView.Adapter<AreasAdapter.AreasViewHolder>(){

    private val currentList: MutableList<Area> = mutableListOf()

    fun newList(newlist: List<Area>){
        val oldsize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldsize)
        currentList.addAll(newlist)
        notifyItemRangeInserted(0, newlist.size)
    }

    inner class AreasViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreasViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.area_fragment, parent, false)
        return AreasViewHolder(view)
    }

    override fun onBindViewHolder(holder: AreasViewHolder, position: Int) {
        holder.itemView.setOnClickListener{
            val currentArea = currentList[holder.adapterPosition]
            onAreaClicked(currentArea.strArea)
        }
        holder.itemView.apply {
            area_text_box.text = currentList[position].strArea
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

}