package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.DashboardFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private var _binding: DashboardFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = DashboardFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCategoriesNavigation.setOnClickListener {
            findNavController().navigate(R.id.CategoryListFragment)
        }
        binding.buttonAreaNavigation.setOnClickListener {
            findNavController().navigate(R.id.AreaListFragment)
        }
        binding.buttonSavedItemsNavigation.setOnClickListener {
            findNavController().navigate(R.id.SavedListFragment)
        }
        binding.buttonIngredientsNavigation.setOnClickListener {
            findNavController().navigate(R.id.IngredientsListFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}