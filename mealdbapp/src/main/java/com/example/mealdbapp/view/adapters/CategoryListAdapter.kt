package com.example.mealdbapp.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.category.Category
import kotlinx.android.synthetic.main.categorylist_fragment.view.category_card_description
import kotlinx.android.synthetic.main.categorylist_fragment.view.category_card_image
import kotlinx.android.synthetic.main.categorylist_fragment.view.category_card_title

class CategoryListAdapter(
    private val onCategoryClicked: (Category) -> Unit
) : RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder>() {

    private val currentList: MutableList<Category> = mutableListOf()


    fun newList(newList: List<Category>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.categorylist_fragment, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val selectedCategory = currentList[holder.adapterPosition]
            onCategoryClicked(selectedCategory)
        }
        holder.itemView.apply {
            category_card_title.text = currentList[position].strCategory ?: "none"
            Glide.with(holder.itemView.rootView.context)
                .load(currentList[position].strCategoryThumb)
                .centerCrop()
                .into(holder.itemView.category_card_image)

            category_card_description.text = currentList[position].strCategoryDescription
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}