package com.example.mealdbapp.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.ingredients.Ingredient
import com.example.mealdbapp.model.local.meal.Meal
import kotlinx.android.synthetic.main.ingredient_list_fragment.view.ingredient_card_description
import kotlinx.android.synthetic.main.ingredient_list_fragment.view.ingredient_card_title
import kotlinx.android.synthetic.main.meal_fragment.view.meal_card_description
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_image
import kotlinx.android.synthetic.main.meal_list_fragment.view.meal_card_title

class IngredientsListAdapter : RecyclerView.Adapter<IngredientsListAdapter.IngredientsListViewHolder>() {

    private val currentList: MutableList<Ingredient> = mutableListOf()

    fun newList(newList: List<Ingredient>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class IngredientsListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.ingredient_list_fragment, parent, false)
        return IngredientsListViewHolder(view)
    }

    override fun onBindViewHolder(holder: IngredientsListViewHolder, position: Int) {

        holder.itemView.apply {
            ingredient_card_title.text = currentList[position].strIngredient
            ingredient_card_description.text = currentList[position].strDescription ?: ""
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}