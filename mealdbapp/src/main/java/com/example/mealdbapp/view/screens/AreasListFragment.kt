package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.view.adapters.AreasAdapter
import com.example.mealdbapp.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.content_main.recyclerView

@AndroidEntryPoint
class AreasListFragment: Fragment() {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val adapter = AreasAdapter({ area ->
        onAreaClicked(area)
    })

    private lateinit var binding: ContentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            binding.categoryMainTitle.text = "Areas"
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    fun initObservers(){
        mainViewModel.getAreas()
        mainViewModel.categoryState.observe(viewLifecycleOwner){ newList ->
            adapter.newList(newlist = newList.areas)
        }
    }

    fun initViews() = with(binding){
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@AreasListFragment.requireContext())
    }

    fun onAreaClicked(area:String){
        mainViewModel.getMealByArea(area)
        findNavController().navigate(R.id.MealListFragment)
    }
}