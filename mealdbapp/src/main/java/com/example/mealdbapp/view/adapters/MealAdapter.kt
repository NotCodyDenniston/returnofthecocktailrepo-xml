package com.example.mealdbapp.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mealdbapp.R
import com.example.mealdbapp.model.local.meal.Meal
import kotlinx.android.synthetic.main.meal_fragment.view.meal_card_description
import kotlinx.android.synthetic.main.meal_fragment.view.meal_card_image
import kotlinx.android.synthetic.main.meal_fragment.view.save_meal_button

class MealAdapter(
    private val onButtonClicked: (Meal) -> Unit,
    private val savedMeals: List<Meal>,
    private val selectedMeal: Meal,
) : RecyclerView.Adapter<MealAdapter.MealViewHolder>() {

    private val currentList: MutableList<Meal> = mutableListOf()

    private val filteredSelectedMeal = savedMeals.find { meal ->
        meal.strMeal == selectedMeal.strMeal
    }
    private val isMealSaved: MutableLiveData<Boolean> = MutableLiveData(
        filteredSelectedMeal?.isSaved == true ||
                savedMeals.contains(filteredSelectedMeal) ?: selectedMeal.isSaved
    )

    fun newList(newList: List<Meal>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class MealViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.meal_fragment, parent, false)
        return MealViewHolder(view)
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        holder.itemView.save_meal_button.setOnClickListener {
            val selectedMeal = currentList[holder.adapterPosition]
            onButtonClicked(selectedMeal)
            isMealSaved.value = !isMealSaved.value!!
        }
        holder.itemView.apply {
            Glide.with(holder.itemView.rootView.context)
                .load(currentList[position].strMealThumb)
                .centerCrop()
                .into(holder.itemView.meal_card_image)
            Log.e(
                "help", "^^^^^^^^^^^^^^^^^" +
                        " ${isMealSaved.value} selected meal: ${selectedMeal.strMeal}" +
                        " filteredSelectedMeal: ${filteredSelectedMeal?.strMeal} savedMeals: ${savedMeals} "
            )
            if ( isMealSaved.value == true) {
            save_meal_button.text = "unsave meal"
        }
            meal_card_description.text = currentList[position].strInstructions
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}