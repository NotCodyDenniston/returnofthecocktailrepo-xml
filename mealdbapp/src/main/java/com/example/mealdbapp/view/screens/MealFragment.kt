package com.example.mealdbapp.view.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet.Layout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mealdbapp.R
import com.example.mealdbapp.databinding.ContentMainBinding
import com.example.mealdbapp.model.local.meal.Meal
import com.example.mealdbapp.view.adapters.CategoryListAdapter
import com.example.mealdbapp.view.adapters.MealAdapter
import com.example.mealdbapp.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.meal_fragment.save_meal_button

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class MealFragment : Fragment() {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val adapter by lazy {MealAdapter({meal ->
        onSaveButtonClicked(meal)
    }, savedMeals = mainViewModel.categoryState.value!!.savedMeals,
        selectedMeal = mainViewModel.categoryState.value!!.selectedMeal[0])}
    private lateinit var binding: ContentMainBinding

    // This property is only valid between onCreateView and
    // onDestroyView.

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            recyclerView.apply {
                initViews()
                initObservers()
            }
            binding.categoryMainTitle.text = mainViewModel.categoryState.value!!.selectedMeal[0].strMeal
            if (binding.categoryMainTitle.text.length > 35){
                binding.categoryMainTitle.textSize = 28F
            }
            binding.categoryMainDescription.text = ""
        }.root

    }

    fun initObservers() {
        mainViewModel.categoryState.observe(viewLifecycleOwner) { newState ->
            adapter.newList(newList = newState.selectedMeal)
        }
    }

    fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@MealFragment.requireContext())

    }

    fun onSaveButtonClicked(meal: Meal){
        mainViewModel.saveMeal(meal)
    }

}

