package com.example.mealdbapp.viewmodel

import com.example.mealdbapp.model.local.areas.Area
import com.example.mealdbapp.model.local.category.Category
import com.example.mealdbapp.model.local.categorymeals.CategoryMeal
import com.example.mealdbapp.model.local.ingredients.Ingredient
import com.example.mealdbapp.model.local.meal.Meal

data class CategoryState(
    val loading: Boolean = false,
    val areasMealListBeingObserved: Boolean = false,

    val categories: List<Category> = emptyList(),
    val meals: List<CategoryMeal> = emptyList(),
    val areas: List<Area> = emptyList(),
    val ingredients: List<Ingredient> = emptyList(),

    val selectedCategory: Category? = null,
    val selectedArea: String = "",
    val selectedMeal: List<Meal> = emptyList(),

    val savedMeals: List<Meal> = emptyList()

)