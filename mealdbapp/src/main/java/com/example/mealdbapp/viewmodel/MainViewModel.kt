package com.example.mealdbapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mealdbapp.model.MealRepo
import com.example.mealdbapp.model.local.category.Category
import com.example.mealdbapp.model.local.meal.Meal
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo: MealRepo
) : ViewModel() {

    private val _categories: MutableLiveData<CategoryState> =
        MutableLiveData(CategoryState())
    val categoryState: LiveData<CategoryState> get() = _categories

    fun getCategories() = runBlocking {
        val newCategories = repo.getCategories()
        _categories.value = _categories.value!!.copy(categories = newCategories.categories)
    }

    fun getCategoryMeals(category: Category) = viewModelScope.launch {
        _categories.value = _categories.value!!.copy(
            selectedCategory = category,
            areasMealListBeingObserved = false
        )
        val newMeals = repo.getMealsInCategory(category.strCategory)
        _categories.value = _categories.value!!.copy(meals = newMeals.meals)
    }

    fun getMeal(meal: String) = runBlocking {
        val newMeal = repo.getMeal(meal)
        _categories.value = _categories.value!!.copy(selectedMeal = newMeal.meals)
    }

    fun getAreas() = viewModelScope.launch {
        val areas = repo.getAreas()
        _categories.value = _categories.value!!.copy(areas = areas.meals)
    }

    fun getMealByArea(area: String) = viewModelScope.launch {
        _categories.value = _categories.value!!.copy(
            selectedArea = area,
            areasMealListBeingObserved = true
        )
        val newMealByArea = repo.getMealByArea(area)
        _categories.value = _categories.value!!.copy(meals = newMealByArea.meals)
    }

    fun saveMeal(meal: Meal) {
        if (_categories.value!!.savedMeals.contains(meal)) {
            val filteredList = _categories.value!!.savedMeals.filter {
                it != meal
            }
            _categories.value = _categories.value!!.copy(savedMeals = filteredList)
        } else {
            _categories.value = _categories.value?.copy(
                savedMeals = _categories.value!!.savedMeals + meal
            )
        }
    }

    fun getIngredients() = viewModelScope.launch {
        val newIngredients = repo.getIngredients()
        with(_categories){
        value = value!!.copy(ingredients = newIngredients.meals)
        }
    }

}

