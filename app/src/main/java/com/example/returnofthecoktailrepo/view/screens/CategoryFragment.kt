package com.example.returnofthecoktailrepo.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.returnofthecoktailrepo.R
import com.example.returnofthecoktailrepo.databinding.ContentMainBinding
import com.example.returnofthecoktailrepo.viewmodel.adapters.CockTailAdapter
import com.example.returnofthecoktailrepo.viewmodel.CockTailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryFragment : Fragment() {

    private val cockTailViewModel: CockTailViewModel by activityViewModels<CockTailViewModel>()
    private val adapter by lazy {
        CockTailAdapter({ category ->
            categoryClicked(category)
        })
    }

    private lateinit var binding: ContentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root

    }

    private fun initObservers() {
        cockTailViewModel.getCategories()
        cockTailViewModel.categoryState.observe(viewLifecycleOwner) {
            adapter.newList(it.drinks)
        }
    }

    private fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@CategoryFragment.requireContext())
    }

    private fun categoryClicked(category: String) {
        cockTailViewModel.getCategoryDrinks(category)
        //may want to use run blocking in viewmodel
        findNavController().navigate(R.id.CategoryDrinksFragment)
    }


}
