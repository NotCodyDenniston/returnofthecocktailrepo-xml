package com.example.returnofthecoktailrepo.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.returnofthecoktailrepo.databinding.ContentMainBinding
import com.example.returnofthecoktailrepo.viewmodel.CockTailViewModel
import com.example.returnofthecoktailrepo.viewmodel.adapters.CategoryDrinksAdapter
import com.example.returnofthecoktailrepo.viewmodel.adapters.DrinksAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DrinksFragment : Fragment() {

    private val cockTailViewModel: CockTailViewModel by activityViewModels<CockTailViewModel>()
    private val adapter by lazy {
        DrinksAdapter({ drink ->
            drinkClicked(drink)
        })
    }

    private lateinit var binding: ContentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ContentMainBinding.inflate(inflater, container, false).apply {
            binding = this
            recyclerView.apply {
                initViews()
                initObservers()
            }
        }.root
    }

    private fun initViews() = with(binding) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager =
            LinearLayoutManager(this@DrinksFragment.requireContext())
    }

    private fun initObservers() {
        cockTailViewModel.drinkState.observe(viewLifecycleOwner) {
            adapter.newList(it.drinks)
        }
    }

    private fun drinkClicked(drinkType: String) {
        cockTailViewModel.getDrinks(drinkType)
        //may want to use run blocking in viewmodel
    }


}
