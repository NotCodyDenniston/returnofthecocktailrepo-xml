package com.example.returnofthecoktailrepo.viewmodel.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.returnofthecoktailrepo.R
import com.example.returnofthecoktailrepo.model.local.Category.Category
import kotlinx.android.synthetic.main.fragment_first.view.textView

class CockTailAdapter(
    private val onCategoryClicked: (String) -> Unit
) : RecyclerView.Adapter<CockTailAdapter.CockTailViewHolder>() {

    private val currentList: MutableList<Category> = mutableListOf()

    fun newList(newList: List<Category>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class CockTailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CockTailViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_first, parent, false)
        return CockTailViewHolder(view)
    }

    override fun onBindViewHolder(holder: CockTailViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val selectedCategory = currentList[holder.adapterPosition]
            onCategoryClicked(selectedCategory.strCategory)
        }
        holder.itemView.apply {
            Log.e("adsf", "!!!!!!!")
            textView.text = currentList[position].strCategory ?: "none"
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}