package com.example.returnofthecoktailrepo.viewmodel.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.returnofthecoktailrepo.R
import com.example.returnofthecoktailrepo.model.local.Drink.Drink
import kotlinx.android.synthetic.main.fragment_first.view.imageView
import kotlinx.android.synthetic.main.fragment_first.view.textView

class DrinksAdapter(
    private val onDrinkTypeClicked: (String) -> Unit
) : RecyclerView.Adapter<DrinksAdapter.CockTailViewHolder>() {

    private val currentList: MutableList<Drink> = mutableListOf()

    fun newList(newList: List<Drink>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    inner class CockTailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CockTailViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_first, parent, false)
        return CockTailViewHolder(view)
    }

    override fun onBindViewHolder(holder: CockTailViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val selectedDrinkType = currentList[holder.adapterPosition]
            onDrinkTypeClicked(selectedDrinkType.strDrink)
        }
        Glide.with(holder.itemView.rootView.context)
            .load(currentList[position].strDrinkThumb)
            .centerCrop()
            .into(holder.itemView.imageView)
        holder.itemView.apply {
            textView.text = currentList[position].strDrink
        }
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}