package com.example.returnofthecoktailrepo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.returnofthecoktailrepo.model.CocktailRepo
import com.example.returnofthecoktailrepo.model.local.Category.CategoryResponse
import com.example.returnofthecoktailrepo.model.local.CategoryDrink.CategoryDrinkResponse
import com.example.returnofthecoktailrepo.model.local.Drink.DrinkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@HiltViewModel
class CockTailViewModel @Inject constructor(
    private val repo: CocktailRepo
) : ViewModel() {

    private val _categories: MutableLiveData<CategoryResponse> =
        MutableLiveData(CategoryResponse())
    val categoryState: LiveData<CategoryResponse> get() = _categories

    private val _categoriesDrink: MutableLiveData<CategoryDrinkResponse> =
        MutableLiveData(CategoryDrinkResponse())
    val categoryDrinkState: LiveData<CategoryDrinkResponse> get() = _categoriesDrink

    private val _drinks: MutableLiveData<DrinkResponse> =
        MutableLiveData(DrinkResponse())
    val drinkState: LiveData<DrinkResponse> get() = _drinks



    fun getCategories() = runBlocking {
        _categories.value = _categories.value!!.copy(drinks = emptyList())
        repo.getCategories()
            .onSuccess { categories ->
                _categories.value = _categories.value!!.copy(drinks = categories)
            }
    }

    fun getCategoryDrinks(category:String) = viewModelScope.launch{
        _categoriesDrink.value = _categoriesDrink.value!!.copy(drinks = emptyList())
        repo.getCategoryDrinks(category)
            .onSuccess { selectedCategory ->
                _categoriesDrink.value = _categoriesDrink.value!!.copy(drinks = selectedCategory)
            }
    }

    fun getDrinks(drinktype:String) = viewModelScope.launch {
        _drinks.value = _drinks.value!!.copy(drinks = emptyList())
        repo.getDrinks(drinktype)
            .onSuccess { drinks ->
                _drinks.value = _drinks.value!!.copy(drinks = drinks)
            }
    }
}


