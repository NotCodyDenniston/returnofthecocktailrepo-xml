package com.example.returnofthecoktailrepo.viewmodel.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.returnofthecoktailrepo.R
import com.example.returnofthecoktailrepo.databinding.FragmentFirstBinding
import com.example.returnofthecoktailrepo.model.local.CategoryDrink.CategoryDrink
import kotlinx.android.synthetic.main.fragment_first.view.imageView
import kotlinx.android.synthetic.main.fragment_first.view.textView
import kotlinx.coroutines.withContext

class CategoryDrinksAdapter(
    private val onDrinkTypeClicked: (String) -> Unit
) : RecyclerView.Adapter<CategoryDrinksAdapter.CockTailViewHolder>() {

    private val currentList: MutableList<CategoryDrink> = mutableListOf()

    fun newList(newList: List<CategoryDrink>) {
        val oldSize = currentList.size
        currentList.clear()
        notifyItemRangeRemoved(0, oldSize)
        currentList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }

    override fun onViewAttachedToWindow(holder: CockTailViewHolder) {
        super.onViewAttachedToWindow(holder)

    }

    inner class CockTailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CockTailViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_first, parent, false)
        return CockTailViewHolder(view)
    }

    override fun onBindViewHolder(holder: CockTailViewHolder, position: Int) {



        holder.itemView.setOnClickListener {
            val selectedDrinkType = currentList[holder.adapterPosition]
            onDrinkTypeClicked(selectedDrinkType.strDrink)
        }
        Glide.with(holder.itemView.rootView.context)
            .load(currentList[position].strDrinkThumb)
            .centerCrop()
            .into(holder.itemView.imageView)
        holder.itemView.apply {
            textView.text = currentList[position].strDrink
        }
    }


    override fun getItemCount(): Int {
        return currentList.size
    }
}