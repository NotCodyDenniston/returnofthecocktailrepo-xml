package com.example.returnofthecoktailrepo.model.di

import com.example.returnofthecoktailrepo.model.remote.CocktailService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    fun providesRetrofit(): CocktailService {
        return CocktailService()
    }
}