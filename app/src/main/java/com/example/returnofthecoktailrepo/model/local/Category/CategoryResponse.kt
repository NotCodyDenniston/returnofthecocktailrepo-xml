package com.example.returnofthecoktailrepo.model.local.Category


import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val drinks: List<Category> = emptyList()
)