package com.example.returnofthecoktailrepo.model

import com.example.returnofthecoktailrepo.model.local.Category.Category
import com.example.returnofthecoktailrepo.model.local.CategoryDrink.CategoryDrink
import com.example.returnofthecoktailrepo.model.local.Drink.Drink
import com.example.returnofthecoktailrepo.model.remote.CocktailService
import javax.inject.Inject

class CocktailRepo @Inject constructor(
    private val service: CocktailService
) {
    suspend fun getCategories(): Result<List<Category>> {
        val categories = service.getCategories()
        return Result.success(categories.body()!!.drinks)
    }

    suspend fun getCategoryDrinks(category: String): Result<List<CategoryDrink>> {
        val drinkCategory = service.getCategoryDrinks(category)
        return Result.success(drinkCategory.body()!!.drinks)
    }

    suspend fun getDrinks(drinkType: String): Result<List<Drink>> {
        val drink = service.getDrink(drinkType)
        return Result.success(drink.body()!!.drinks)
    }
}

