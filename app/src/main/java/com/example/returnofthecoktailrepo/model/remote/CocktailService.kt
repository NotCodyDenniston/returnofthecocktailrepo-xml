package com.example.returnofthecoktailrepo.model.remote

import com.example.returnofthecoktailrepo.model.local.Category.CategoryResponse
import com.example.returnofthecoktailrepo.model.local.CategoryDrink.CategoryDrinkResponse
import com.example.returnofthecoktailrepo.model.local.Drink.DrinkResponse
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {


    @GET(CATEGORIES_ENDPOINT)
    suspend fun getCategories(@Query("c") c: String = "list"): Response<CategoryResponse>

    @GET(CATEGORY_DRINKS_ENDPOINT)
    suspend fun getCategoryDrinks(@Query("c") c:String? = "Ordinary_Drink"): Response<CategoryDrinkResponse>

    @GET(DRINKS_ENDPOINT)
    suspend fun getDrink(@Query("s") s:String? = "margarita"): Response<DrinkResponse>

    companion object {
        private const val CATEGORIES_ENDPOINT = "list.php"
        private const val CATEGORY_DRINKS_ENDPOINT = "filter.php"
        private const val DRINKS_ENDPOINT = "search.php"

        @OptIn(ExperimentalSerializationApi::class)
        operator fun invoke(): CocktailService {
            val base_url = "https://www.thecocktaildb.com/"
            val version = "api/json/v1/1/"
            val mediaType = MediaType.get("application/json")

            val retrofit: Retrofit = Retrofit
                .Builder()
                .baseUrl(base_url + version)
                .addConverterFactory(Json.asConverterFactory(mediaType))
                .build()
            return retrofit.create()
        }
    }
}
