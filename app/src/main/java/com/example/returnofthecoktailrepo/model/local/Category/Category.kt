package com.example.returnofthecoktailrepo.model.local.Category

import kotlinx.serialization.Serializable

@Serializable
data class Category(
    val strCategory: String = ""
)