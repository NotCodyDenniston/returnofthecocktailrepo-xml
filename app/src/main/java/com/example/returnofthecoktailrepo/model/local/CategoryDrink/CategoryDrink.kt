package com.example.returnofthecoktailrepo.model.local.CategoryDrink

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrink(
    val idDrink: String = "",
    val strDrink: String = "",
    val strDrinkThumb: String = ""
)