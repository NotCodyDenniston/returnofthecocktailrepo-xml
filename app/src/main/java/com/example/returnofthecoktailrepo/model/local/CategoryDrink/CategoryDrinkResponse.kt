package com.example.returnofthecoktailrepo.model.local.CategoryDrink

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrinkResponse(
    val drinks: List<CategoryDrink> = emptyList()
)