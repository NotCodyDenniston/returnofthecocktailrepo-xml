package com.example.returnofthecoktailrepo.model.local.Drink


import kotlinx.serialization.Serializable

@Serializable
data class DrinkResponse(
    val drinks: List<Drink> = emptyList()
)