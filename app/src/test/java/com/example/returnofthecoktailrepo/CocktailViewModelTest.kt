package com.example.returnofthecoktailrepo

import com.example.returnofthecoktailrepo.model.CocktailRepo
import com.example.returnofthecoktailrepo.model.local.Category.Category
import com.example.returnofthecoktailrepo.model.local.Category.CategoryResponse
import com.example.returnofthecoktailrepo.viewmodel.CockTailViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.RegisterExtension

class CocktailViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @RegisterExtension
    val testExtension = CoroutineTestExtension()

    private val repo: CocktailRepo = mockk()
    private val viewModel: CockTailViewModel = CockTailViewModel(repo)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInitialValue() = runTest(testExtension.dispatcher) {
        //Given
        val state = viewModel.categoryState.value
        val expected = CategoryResponse
        //When
        //Then
        Assertions.assertFalse(state?.drinks == null)
        Assertions.assertTrue(state?.drinks?.isEmpty() == true)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testUpdatedValue() = runTest(testExtension.dispatcher) {
        //Given
        val expectedValue = { listOf(Category(strCategory = "Gin Drinks")) }
        coEvery { repo.getCategories() } coAnswers { Result.success(expectedValue()) }
        //When
        viewModel.getCategories()
        //Then
        val state = viewModel.categoryState.value
        Assertions.assertFalse(state?.drinks == null)
        Assertions.assertTrue(state?.drinks?.isEmpty() == false)
        // checks to see if drinks contains a category with gin drinks as the name.
        state?.drinks?.contains(Category(strCategory = "Gin Drinks"))
            ?.let { Assertions.assertTrue(it) }
    }
}